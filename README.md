# AVDS.PureCloudJSLibrary
This JS library facilitates connecting to, and querying data from, the PureCloud API.

#Description
This tool is designed to allow you to access PureCloud data using as little code as possible. That said, you are still required to pass in the options for the query as the options often change project to project.

#Instructions
1. Download the file you want to use (full version or min version)
2. Place it in your project folder
3. Reference the file in the head using the SCRIPT tag

### Calling the functions
The first step will be logging into the PureCloud API. You'll need to call the function and pass the Client token and the page's URI    
```
    pureCloudLogin([token], [uri]);
```

#####**At this point you can call any of the functions below.**

[Get the members of a Queue:](https://developer.mypurecloud.com/api/rest/v2/routing/index.html#getRoutingQueuesQueueIdUsers)
```
const results = pureCloudRoutingQueueUsers([QueueID], [Options]);
```

[Get a list of Queues:](https://developer.mypurecloud.com/api/rest/v2/routing/index.html#getRoutingQueues)
```
const results = pureCloudRoutingQueues([Options]);
```

[Get summary of conversations \(Good for Queue metrics\):](https://developer.mypurecloud.com/api/rest/v2/analytics/index.html#postAnalyticsConversationsAggregatesQuery)
```
const results = pureCloudConversationsAggregates([Options]);
```

[Get the details of a conversation:](https://developer.mypurecloud.com/api/rest/v2/analytics/index.html#postAnalyticsConversationsDetailsQuery)
```
const results = pureCloudConversationsDetailsQuery([Options]);
```

[Get information on user statuses and states:](https://developer.mypurecloud.com/api/rest/v2/users/index.html#postAnalyticsUsersAggregatesQuery)
```
const results = pureCloudUsersAggregatesQuery([Options]);
```

[Get details about a User\'s account:](https://developer.mypurecloud.com/api/rest/v2/users/index.html#getUsers)
```
const results = pureCloudUsers([Options]);
```

[Get conversation details that includes attributes:](https://developer.mypurecloud.com/api/rest/v2/conversations/index.html#getConversationsConversationId)
```
const results = pureCloudConversations([ConversationId]);
```

#References
* [PureCloud Developer Portal](https://developer.mypurecloud.com)
* [API Documentation](https://developer.mypurecloud.com/api/rest/v2/)
* [PureCloud API Explorer](https://developer.mypurecloud.com/developer-tools/#/api-explorer)
* [PureCloud Metrics](https://developer.mypurecloud.com/api/rest/v2/analytics/metrics.html)

#Author
Sam Marziano

#Version
2018.08.09 - Version 1.0 - avds-pclib.js
2018.08.09 - Version 1.0 - avds-pclib-min.js