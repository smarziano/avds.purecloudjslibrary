const platformClient=require('platformClient');const client=platformClient.ApiClient.instance;const analyticsApi=new platformClient.AnalyticsApi();const routingApi=new platformClient.RoutingApi();const usersApi=new platformClient.UsersApi();const conversationsApi=new platformClient.ConversationsApi();client.setEnvironment('mypurecloud.com');client.setPersistSettings(!0,'test_app');function pureCloudLogin(tkn,uri){let loggedIn=!1;if(tkn&&tkn.length>0){if(uri&&uri.length>0){client.loginImplicitGrant(clientId,redirectUri).then(()=>{console.log('Logged In to PureCloud!');loggedIn=!0}).catch((err)=>{console.error(err)})}
else{console.log('There is something wrong with the URI passed in. URI: '+uri)}}
else{console.log('There is something wrong with the token passed in. Token: '+tkn)}
return loggedIn}
function pureCloudRoutingQueueUsers(qid,o){const results;if(o&&o.length>0){if(qid&&qid.length>0){routingApi.getRoutingQueueUsers(qid,o).then(results=>{return results}).catch(e=>{console.error(e)})}
else{console.log('There is something wrong with the Queue Id passed in. QID: '+qid)}}
else{console.log('There is something wrong with the options passed in:');console.log(o)}}
function pureCloudRoutingQueues(o){if(o&&o.length>0){routingApi.getRoutingQueues(o).then(results=>{return results}).catch(e=>{console.error(e)})}
else{console.log('There is something wrong with the options passed in:');console.log(o)}}
function pureCloudConversationsAggregates(o){if(o&&o.length>0){analyticsApi.postAnalyticsConversationsAggregatesQuery(o).then(results=>{return results}).catch(e=>{console.error(e)})}
else{console.log('There is something wrong with the options passed in:');console.log(o)}}
function pureCloudConversationsDetailsQuery(o){if(o&&o.length>0){analyticsApi.apiAnalytics.postAnalyticsConversationsDetailsQuery(o).then(results=>{return results}).catch(e=>{console.error(e)})}
else{console.log('There is something wrong with the options passed in:');console.log(o)}}
function pureCloudUsersAggregatesQuery(o){if(o&&o.length>0){usersApi.postAnalyticsUsersAggregatesQuery(o).then(results=>{return results}).catch(e=>{console.error(e)})}
else{console.log('There is something wrong with the options passed in:');console.log(o)}}
function pureCloudUsers(o){if(o&&o.length>0){usersApi.getUsers(o).then(results=>{return results}).catch(e=>{console.error(e)})}
else{console.log('There is something wrong with the options passed in:');console.log(o)}}
function pureCloudConversations(cid){if(cid&&cid.length>0){conversationsApi.getConversation(cid).then(results=>{return results}).catch(e=>{console.error(e)})}
else{console.log('There is something wrong with the options passed in:');console.log(o)}}