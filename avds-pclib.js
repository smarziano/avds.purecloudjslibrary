// Set purecloud objects
const platformClient = require('platformClient');
const client = platformClient.ApiClient.instance;
const analyticsApi = new platformClient.AnalyticsApi();
const routingApi = new platformClient.RoutingApi();
const usersApi = new platformClient.UsersApi();
const conversationsApi = new platformClient.ConversationsApi();

// Set PureCloud settings
client.setEnvironment('mypurecloud.com');
client.setPersistSettings(true, 'test_app');

// Takes in the token(tkn) & URI(uri), & returns a bool
function pureCloudLogin(tkn, uri) {
  let loggedIn = false;
  if (tkn && tkn.length > 0) {
    if (uri && uri.length > 0) {
      client.loginImplicitGrant(clientId, redirectUri)
      .then(() => {
        console.log('Logged In to PureCloud!');
        loggedIn = true;
      })
      .catch((err) => { console.error(err); });
    }
    else {
      console.log('There is something wrong with the URI passed in. URI: ' + uri);
    }
  }
  else {
    console.log('There is something wrong with the token passed in. Token: ' + tkn);
  }

  return loggedIn;
}

// Uses Routing Queue Users API
// Takes in options Object("o") & queueId("qid"), and returns the results("results")
function pureCloudRoutingQueueUsers(qid, o) {
  const results;
  if (o && o.length > 0) {
    if (qid && qid.length > 0) {
      routingApi.getRoutingQueueUsers(qid, o)
      .then(results => {
        return results;
      })
      .catch(e => { console.error(e); });
    }
    else {
      console.log('There is something wrong with the Queue Id passed in. QID: ' + qid);
    }
  }
  else {
    console.log('There is something wrong with the options passed in:');
    console.log(o);
  }
}

// Uses the Routing Queues API
// Takes in options Object("o") & returns the results Object("results")
function pureCloudRoutingQueues(o) {
  if (o && o.length > 0) {
    routingApi.getRoutingQueues(o)
    .then(results => {
      return results;
    })
    .catch( e => { console.error(e); });
  }
  else {
    console.log('There is something wrong with the options passed in:');
    console.log(o);
  }
}

// Uses Conversation Aggregates API
// Takes options Object("o") & returns the results Object("results")
function pureCloudConversationsAggregates(o) {
  if (o && o.length > 0) {
    analyticsApi.postAnalyticsConversationsAggregatesQuery(o)
    .then(results => {
      return results;
    })
    .catch( e => { console.error(e); });
  }
  else {
    console.log('There is something wrong with the options passed in:');
    console.log(o);
  }
}

// Uses Conversation Details Query
// Takes options Object("o") & returns the results Object("results")
function pureCloudConversationsDetailsQuery(o) {
  if (o && o.length > 0) {
    analyticsApi.apiAnalytics.postAnalyticsConversationsDetailsQuery(o)
    .then(results => {
      return results;
    })
    .catch( e => { console.error(e); });
  }
  else {
    console.log('There is something wrong with the options passed in:');
    console.log(o);
  }
}

// Uses Users Aggregates Query API
// Takes options Object("o") & returns the results Object("results")
function pureCloudUsersAggregatesQuery(o) {
  if (o && o.length > 0) {
    usersApi.postAnalyticsUsersAggregatesQuery(o)
    .then(results => {
      return results;
    })
    .catch( e => { console.error(e); });
  }
  else {
    console.log('There is something wrong with the options passed in:');
    console.log(o);
  }
}

// Uses Users API
// Takes options Object("o") & returns the results Object("results")
function pureCloudUsers(o) {
  if (o && o.length > 0) {
    usersApi.getUsers(o)
    .then(results => {
      return results;
    })
    .catch( e => { console.error(e); });
  }
  else {
    console.log('There is something wrong with the options passed in:');
    console.log(o);
  }
}

// Uses Conversation API
// Takes conversation id("cid") & returns the results Object("results")
function pureCloudConversations(cid) {
  if (cid && cid.length > 0) {
    conversationsApi.getConversation(cid)
    .then(results => {
      return results;
    })
    .catch( e => { console.error(e); } );
  }
  else {
    console.log('There is something wrong with the options passed in:');
    console.log(o);
  }
}
